
const parametroController = require('./src/controller/ParametroController');

module.exports.listar = async (event, context) => {
    const currentMethod = event.httpMethod;
    const queryStringParams = event.queryStringParameters;

    const bodyParams = event.body;
    const ruta = event.bodyParams;
    console.log('****  handler.js ********');
    console.log( ruta);
    //['X-Forwarded-For']
    console.log('**** event ****');
    console.log(event);
    console.log('****  currentMethod :'+ currentMethod);
    console.log('****  queryStringParams :'+ queryStringParams);
    console.log(currentMethod);
    switch (currentMethod) {
        case 'POST':
            return parametroController.listar(JSON.parse(bodyParams));
        case 'GET':
            return parametroController.listarTodo();

        default:
            return 'method not allowed';
    }
};
