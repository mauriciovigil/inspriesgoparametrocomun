const parametroService = require("../service/ParametroService");
const response = require("../response");

async function test(req) {
    return response.successResponse("success");
}

async function listar(params) {
    const Parametro = await parametroService.listar(params);

    if (Parametro.result) {
        console.log('xxxxxxxxx');
        console.log(Parametro.data);
        console.log('xxxxxxxxx');
        return response.successResponse("success", Parametro.data);
    } else {
        return response.errorResponse("error", Parametro.errores);
    }
}

async function listarTodo() {
    console.log('**** parametro controller ****');
    const ParametroAll = await parametroService.listarTodo();

    if (ParametroAll.result) {
        return response.successResponse("success", ParametroAll.data);
    } else {
        return response.errorResponse("error", ParametroAll.errores);
    }
}




module.exports = {
    test, listar, listarTodo
}