const dynamoDbClient = require('../db/config');

const DATAPARAMETRO_TABLE = process.env.DATAPARAMETRO_TABLE;
const PARAMETRO_TABLE = process.env.PARAMETRO_TABLE;
const estado="01";
const fecfinvig="01/01/2100";
let params;
var GrupoParametro;
var y;
async function listar(req) {

    console.log('**** parametroService');
    console.log('**** codGrupo :'+ req.codGrupo);
    console.log('**** codParametro :'+ req.codParametro);
    console.log('**** estado :'+ estado);
    console.log('**** fecfinvig :'+ fecfinvig);
    console.log('**** DATAPARAMETRO_TABLE :'+ process.env.DATAPARAMETRO_TABLE);

    if(req.codParametro && req.codGrupo) {
        
      GrupoParametro = await  getParametro(req);
      console.log('------ ini getParametro------');
      if (GrupoParametro.result==false) return GrupoParametro;
      delete GrupoParametro.result;
      for (y of GrupoParametro.data) {
                               delete y.fec_inicat;
                               delete y.fec_fincat;
                               delete y.fec_fincatorig;
                              // delete y.des_catalogo;
                              // delete y.des_acronimo;
                               delete y.cod_depen;
                               delete y.ind_acceso;
                               delete y.cod_estado;
                               delete y.cod_usuregis;
                               delete y.cod_usumodif;
                               delete y.fec_regis;
                               delete y.cod_usumodif;
                               delete y.fec_modif;
                               delete y.num_transaccion;
                               delete y.ind_regactual;
       }
      console.log(GrupoParametro);
      console.log('------ fin getParametro------');
      
      var listaParametro = await  getDataParametro(req);
      if (listaParametro.result==false) return listaParametro;
      delete listaParametro.result;
       for (y of listaParametro.data) {
                               delete y.cod_usuregis;
                               delete y.ind_regactual;
                               delete y.fec_inidatcat;
                               delete y.cod_estado;
                               delete y.num_transaccion;
                               delete y.des_acronimo;
                               delete y.fec_inicat;
                               delete y.fec_findatcat;
                               delete y.fec_finvigorig;
                               delete y.fec_regis;
                               delete y.fec_modif;
                               delete y.cod_usumodif;
                               delete y.cod_catalogo;
       }
      console.log(listaParametro);
      
    // var output1 = Object.assign(GrupoParametro,listaParametro);
     
     var output1 = {GrupoParametro,listaParametro};
     return {result: true,data:output1};
        

   }else if (req.codGrupo){
       
      GrupoParametro = await  getParametro(req);
      
      
      console.log('------ ini getParametro------');
      
      if (GrupoParametro.result==false) return GrupoParametro;
      delete GrupoParametro.result;
      for (y of GrupoParametro.data) {
                               delete y.fec_inicat;
                               delete y.fec_fincat;
                               delete y.fec_fincatorig;
                              // delete y.des_catalogo;
                              // delete y.des_acronimo;
                               delete y.cod_depen;
                               delete y.ind_acceso;
                               delete y.cod_estado;
                               delete y.cod_usuregis;
                               delete y.cod_usumodif;
                               delete y.fec_regis;
                               delete y.cod_usumodif;
                               delete y.fec_modif;
                               delete y.num_transaccion;
                               delete y.ind_regactual;
       }
      console.log(GrupoParametro);
      console.log('------ fin getParametro------');
      
      var listaParametro = await  listarDataParametro(req.codGrupo);
      if (listaParametro.result==false) return listaParametro;
      delete listaParametro.result;
       for (y of listaParametro.data) {
                               delete y.cod_usuregis;
                               delete y.ind_regactual;
                               delete y.fec_inidatcat;
                               delete y.cod_estado;
                               delete y.num_transaccion;
                               delete y.des_acronimo;
                               delete y.fec_inicat;
                               delete y.fec_findatcat;
                               delete y.fec_finvigorig;
                               delete y.fec_regis;
                               delete y.fec_modif;
                               delete y.cod_usumodif;
                               delete y.cod_catalogo;
       }
      console.log(listaParametro);
      
    // var output1 = Object.assign(GrupoParametro,listaParametro);
     
     var output1 = {GrupoParametro,listaParametro};
     return {result: true,data:output1};

 
   }else {
       return {result: false,errores:'parametros no reconocidos'};
   }
        
    
}

async function listarTodo() {

    console.log('**** listarTodo');
    
    var items = await listarParametro();
    var parametro;
    var itemstot;
    var x;
    var codGrupo;
    var y;
    var dataparametro;
    try {
            console.log('***1***');
            if (items.result==false){
                return {result: false,errores:'No se encontraron datos en tabla:'+DATAPARAMETRO_TABLE};
            }
            var grupo = [];
            var desgrupo = [];
            var json = [];
    
           for (x of items.data) {
              parametro = {cod_catalogo:x.cod_catalogo, des_catalogo:x.des_catalogo};
              codGrupo = x.cod_catalogo;
              grupo.push(codGrupo);
              desgrupo.push(x.des_catalogo);
              console.log('***codGrupo***'+codGrupo);
           }

           for (let i = 0; i < grupo.length; i++) {
               parametro = {cod_catalogo:grupo [i], des_catalogo:desgrupo [i]};
               dataparametro = await listarDataParametro(grupo [i]);
                if (dataparametro.result==true){
                       for (y of dataparametro.data) {
                               delete y.cod_usuregis;
                               delete y.ind_regactual;
                               delete y.fec_inidatcat;
                               delete y.cod_estado;
                               delete y.num_transaccion;
                               delete y.des_acronimo;
                               delete y.fec_inicat;
                               delete y.fec_findatcat;
                               delete y.fec_finvigorig;
                               delete y.fec_regis;
                               delete y.fec_modif;
                               delete y.cod_usumodif;
                               delete y.cod_catalogo;
                       }
                }else{
                    var error= {des_datacat:'No se encontraron datos',des_corta:'No se encontraron datos',cod_datacat:'0'};
                    itemstot =  {itemstot,parametro, error };
                }
                delete dataparametro.result;
               json.push({parametro, dataparametro});
               
            }
            var output;
            var output1={};
            for (let i = 0; i < grupo.length; i++) {
                    var coche = json[i];
                    var jsonCompleto = JSON.stringify(coche); 
                    jsonCompleto = jsonCompleto.replace('parametro','parametro'+i);
                    jsonCompleto = jsonCompleto.replace('dataparametro','dataparametro'+i);
                    json[i] =  JSON.parse(jsonCompleto);

                  output1 = Object.assign(output1,json[i]); 
            }
           console.log('***output fin***');
           console.log(output1);


          console.log('***fin***'); 
         // console.log(itemstot); 
    } catch (err) {
        return {result: false,errores:'Error:'+err};
    }
    //console.log(itemstot ['itemstot']);
    return {result: true,data:output1};

   }
   
 
async function listarDataParametro(codGrupo) {

    console.log('**** listarDataParametro');
    console.log('**** codGrupo :'+ codGrupo);
    console.log('**** DATAPARAMETRO_TABLE :'+ process.env.DATAPARAMETRO_TABLE);
          params = {
                TableName: DATAPARAMETRO_TABLE,
                ExpressionAttributeValues: {
                    ':cod_catalogo': codGrupo 
                },
                KeyConditionExpression: 'cod_catalogo =:cod_catalogo',
            };
    
       try {
            var dataparametro = await dynamoDbClient.query(params).promise() ;
            console.log('-----****------');
            console.log(dataparametro);

                return {result: true,data:dataparametro ['Items']};
        } catch (err) {
                return {result: false,errores:'Error:'+err};
        }

   }
 
   async function listarParametro() {

    console.log('**** PARAMETRO_TABLE :'+ process.env.PARAMETRO_TABLE);
        params = {
                TableName: PARAMETRO_TABLE
        };
        try {
            var dataParametro = await dynamoDbClient.scan(params).promise();
            console.log('-----****------');
            console.log(dataParametro);

                return {result: true,data:dataParametro ['Items']};
        } catch (err) {
                return {result: false,errores:'Error:'+err};
        }
   }
   
   async function getParametro(req) {

    console.log('**** getParametro');
    console.log('**** codGrupo :'+ req.codGrupo);
    console.log('**** PARAMETRO_TABLE :'+ process.env.PARAMETRO_TABLE);
          params = {
                TableName: PARAMETRO_TABLE,
                ExpressionAttributeValues: {
                    ':cod_catalogo':  req.codGrupo
                },
                KeyConditionExpression: 'cod_catalogo =:cod_catalogo',
            };
    
       try {
                var dataparametro = await dynamoDbClient.query(params).promise() ;
                  if(dataparametro ['Count'] =="0"){
                    return {result: false,errores:'No se encontraron registros para el grupo:'+PARAMETRO_TABLE}; 
                 }
                return {result: true,data:dataparametro ['Items']};
        } catch (err) {
                return {result: false,errores:'Error:'+err};
        }

   }
   
   async function getDataParametro(req) {

    console.log('**** getDataParametro');
    console.log('**** codGrupo :'+ req.codGrupo);
    console.log('**** codParametro :'+ req.codParametro);
    console.log('**** DATAPARAMETRO_TABLE :'+ process.env.DATAPARAMETRO_TABLE);
          params = {
                TableName: DATAPARAMETRO_TABLE,
                ExpressionAttributeValues: {
                    ':cod_catalogo': req.codGrupo ,
                    ':cod_datacat':  req.codParametro
                },
                KeyConditionExpression: 'cod_catalogo =:cod_catalogo and cod_datacat = :cod_datacat',
            };
    
       try {
            var dataparametro = await dynamoDbClient.query(params).promise() ;
            console.log('-----****------');
            console.log(dataparametro);

                return {result: true,data:dataparametro ['Items']};
        } catch (err) {
                return {result: false,errores:'Error:'+err};
        }

   }

module.exports = {
    listar,listarParametro,listarDataParametro,listarTodo,getParametro,getDataParametro
}